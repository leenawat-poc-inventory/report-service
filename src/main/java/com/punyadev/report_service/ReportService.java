package com.punyadev.report_service;

import com.googlecode.jthaipdf.jasperreports.engine.export.ThaiJRPdfExporter;
import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
//import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ReportService {

    private final ApplicationContext context;
//    private final DataSource springDataSource;
//    private final JdbcTemplate jdbcTemplate;

    private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HHmmss");


    public ReportService(
            ApplicationContext context// ,
//                         DataSource dataSource ,
//                         JdbcTemplate jdbcTemplate
    ) {
        this.context = context;
//        this.springDataSource = dataSource;
//        this.jdbcTemplate = jdbcTemplate;
    }
    public void hello1(HttpServletResponse response) throws JRException, IOException {
        // 1 file jasper
        String jasperFilename = "classpath:reports-in-resources/report-empty-param-empty-datasource-in-resources.jasper";
        Resource resource = context.getResource(jasperFilename);
        InputStream inputStream = resource.getInputStream();

        // 2 map
        Map<String, Object> map = new HashMap<>();

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, map, dataSource);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new JRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello2(HttpServletResponse response) throws JRException, IOException {
        // 1 file jasper
        String jrxmlFilename = "classpath:reports-in-resources/report-empty-param-empty-datasource-in-resources.jrxml";
        Resource resource = context.getResource(jrxmlFilename);
        InputStream inputStream = resource.getInputStream();
        JasperReport jasperReport = JasperCompileManager.compileReport(inputStream); // เพิ่มมาจาก hello1

        // 2 map
        Map<String, Object> map = new HashMap<>();

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, dataSource); // แตกต่างจากข้อ hello1

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new JRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello3(HttpServletResponse response) throws JRException, IOException {
        // 1 file jasper
        String jasperFilename = "reports-in-root/report-empty-param-empty-datasource-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFilename, map, dataSource);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new JRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello4(HttpServletResponse response) throws JRException, IOException {
        // 1 file jasper
        File jrxmlFile = new File("reports-in-root/report-empty-param-empty-datasource-in-root.jrxml");
        InputStream inputStream = new FileInputStream(jrxmlFile); // เพิ่มมาจาก hello3
        JasperReport jasperReport = JasperCompileManager.compileReport(inputStream); // เพิ่มมาจาก hello3

        // 2 map
        Map<String, Object> map = new HashMap<>();

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, dataSource); // แตกต่างจากข้อ hello3

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new JRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello5(HttpServletResponse response) throws JRException, IOException {
        // 1 file jasper
        String jasperFilename = "reports-in-root/report-empty-param-empty-datasource-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFilename, map, dataSource);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new ThaiJRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello6(HttpServletResponse response, String headerName) throws JRException, IOException {
        // 1 file jasper
        String jasperFilename = "reports-in-root/report-with-param-empty-datasource-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();
        map.put("pHeaderName", headerName);

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFilename, map, dataSource);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new ThaiJRPdfExporter();

        // 6.1  jasperPrint 1 รายงาน
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);

        // 6.2 jasperPrint 2 รายงานขึ้นไป
        // exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrint));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }

    public void hello7(HttpServletResponse response, String headerName) throws JRException, IOException {
        // 1 file jasper
        String jasperLandscapeFilename = "reports-in-root/report-with-param-empty-datasource-landscape-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();
        map.put("pHeaderName", headerName);

        // 3 data source
        JREmptyDataSource dataSource = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrintLandscape = JasperFillManager.fillReport(jasperLandscapeFilename, map, dataSource);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new ThaiJRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrintLandscape);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }


    public void hello8(HttpServletResponse response, String headerName) throws JRException, IOException {
        // 1 file jasper
        String jasperPortraitFilename = "reports-in-root/report-with-param-empty-datasource-in-root.jasper";
        String jasperLandscapeFilename = "reports-in-root/report-with-param-empty-datasource-landscape-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();
        map.put("pHeaderName", headerName);

        // 3 data source
        JREmptyDataSource dataSource1 = new JREmptyDataSource();
        JREmptyDataSource dataSource2 = new JREmptyDataSource();

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrintPortrait = JasperFillManager.fillReport(jasperPortraitFilename, map, dataSource1);
        JasperPrint jasperPrintLandscape = JasperFillManager.fillReport(jasperLandscapeFilename, map, dataSource2);

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new ThaiJRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST,  Arrays.asList(jasperPrintPortrait, jasperPrintLandscape));
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }


//    public void hello9(HttpServletResponse response, String headerName) throws JRException, IOException, SQLException {
//        // 1 file jasper
//        String jasperPortraitFilename = "reports-in-root/report-with-param-sqlite-datasource-in-root.jasper";
//
//        // 2 map
//        Map<String, Object> map = new HashMap<>();
//        map.put("pHeaderName", headerName);
//
//        // 3 ใช้ connection แทน data source
//        Connection connection = springDataSource.getConnection();
//
//        // 4 fileReport with jasperFilename, map, dataSource
//        JasperPrint jasperPrintPortrait = JasperFillManager.fillReport(jasperPortraitFilename, map, connection);
//
//        // 5 set response type and response filename
//        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
//        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");
//
//        // 6 set export and export
//JRExporter exporter = new ThaiJRPdfExporter();
//        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrintPortrait);
//        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//        exporter.exportReport();
//    }


//    public void hello10(HttpServletResponse response, String headerName) throws JRException, IOException, SQLException {
//        // 1 file jasper
//        String jasperPortraitFilename = "reports-in-root/report-with-param-sqlite-datasource-in-root.jasper";
//
//        // 2 map
//        Map<String, Object> map = new HashMap<>();
//        map.put("pHeaderName", headerName);
//
//        // 3 ใช้ connection แทน data source
//        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from employee");
//        //ทดสอบเพิ่ม list เข้าไปอีก 1 ตัวเพื่อให้แตกต่างจากที่มีเดินใน db
//        Map<String, Object> map2 = new HashMap<String, Object>();
//        map2.put("name", "leenawat");
//        map2.put("lastname", "papahom");
//        map2.put("age", "36");
//        list.add(map2);
//
//        // 4 fileReport with jasperFilename, map, dataSource
//        JasperPrint jasperPrintPortrait = JasperFillManager.fillReport(jasperPortraitFilename, map, new JRBeanCollectionDataSource(list));
//
//        // 5 set response type and response filename
//        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
//        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");
//
//        // 6 set export and export
//
//        JRExporter exporter = new ThaiJRPdfExporter();
//        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrintPortrait);
//        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//        exporter.exportReport();
//    }


    public void hello11(HttpServletResponse response, String headerName) throws JRException, IOException, SQLException {
        // 1 file jasper
        String jasperPortraitFilename = "reports-in-root/report-with-param-sqlite-datasource-in-root.jasper";

        // 2 map
        Map<String, Object> map = new HashMap<>();
        map.put("pHeaderName", headerName);

        // 3 ใช้ connection แทน data source
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("name", "นายมั่นใจสุด ");
        map2.put("lastname", "นักสู้");
        map2.put("age", "36");
        list.add(map2);

        map2 = new HashMap<String, Object>();
        map2.put("name", "Mr.John");
        map2.put("lastname", "Smith");
        map2.put("age", "66");
        list.add(map2);

        // 4 fileReport with jasperFilename, map, dataSource
        JasperPrint jasperPrintPortrait = JasperFillManager.fillReport(jasperPortraitFilename, map, new JRBeanCollectionDataSource(list));

        // 5 set response type and response filename
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        response.setHeader("Content-disposition", "attachment; filename=" + dateFormat.format(new Date()) + ".pdf");

        // 6 set export and export
        JRExporter exporter = new ThaiJRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT,  jasperPrintPortrait);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
        exporter.exportReport();
    }
}
