package com.punyadev.report_service;

import jakarta.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.SQLException;

@RestController
public class ReportController {
    private final ReportService reportService;

    public ReportController(ReportService reportService){
        this.reportService = reportService;
    }

    @GetMapping(path = "/")
    public String index() {
        return "hello world";
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน resources เป็น input Stream
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ต้อง restart project ตัว spring boot ถึงจะ update ตาม
    // ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น
    @GetMapping(path = "/1")
    public void hello1(HttpServletResponse response) throws JRException, IOException {
        reportService.hello1(response);
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jrxml จากใน resources เป็น input Stream
    // ใช้ program compile จาก .jrxml ใน code
    // ไม่ต้องใช้ .jasper
    // โปรแกรมอาจจะทำงานได้ช้ากว่าใช้แบบ jasper
    // ต้อง restart project ตัว spring boot ถึงจะ update ตาม
    // ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น
    @GetMapping(path = "/2")
    public void hello2(HttpServletResponse response) throws JRException, IOException {
        reportService.hello2(response);
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น
    @GetMapping(path = "/3")
    public void hello3(HttpServletResponse response) throws JRException, IOException {
        reportService.hello3(response);
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jrxml จากใน root เป็น input Stream
    // ต้อง compile จาก .jrxml ใน code
    // ไม่ต้องใช้ .jasper
    // ไม่ต้อง restart project
    // ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น
    @GetMapping(path = "/4")
    public void hello4(HttpServletResponse response) throws JRException, IOException {
        reportService.hello4(response);
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
    @GetMapping(path = "/5")
    public void hello5(HttpServletResponse response) throws JRException, IOException {
        reportService.hello5(response);
    }

    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
    @GetMapping(path = "/6")
    public void hello6(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException {
        reportService.hello6(response, headerName);
    }

    // landscape
    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
    @GetMapping(path = "/7")
    public void hello7(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException {
        reportService.hello7(response, headerName);
    }

    // portraint and landscape
    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
    @GetMapping(path = "/8")
    public void hello8(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException {
        reportService.hello8(response, headerName);
    }

    // ใช้ sqlite เป็นฐานข้อมูล อันเดียวกันกับที่ JasperStudio ใช้
    // ง่ายต่อการทดสอบ เพราะใน jasper studio เห็นอะไร รันใน spirng ก็เห็นอันเดียวกัน
    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
//    @GetMapping(path = "/9")
//    public void hello9(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException, SQLException {
//        reportService.hello9(response, headerName);
//    }

    // ใช้ JdbcTemplate ดึงข้อมูล แล้วค่อยส่งเป็น List ไปที่ jasper
    // ทดสอบยากกว่าแบบ 9
    // ข้อดีคือสามารถเอาข้อมูลมาจากแหล่งไหนก็ได้ ไม่ได้ผูกดิดกับ datasource ที่เป็นอันเดียวกันกับใน jasper
    // ในการเทสโดย jasper studio เอง เราก็สามารถ mock เป็น db มาเทสได้เหมือนกัน ขอแค่มี field เหมือนกัน
    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
//    @GetMapping(path = "/10")
//    public void hello10(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException, SQLException {
//        reportService.hello10(response, headerName);
//    }

    // ใช้ jasper อันเดิมจากที่ดึงจาก db แต่เปลี่ยนเป็น list ที่สร้างเอง
    // ทดสอบยากกว่าแบบ 9
    // ข้อดีคือสามารถเอาข้อมูลมาจากแหล่งไหนก็ได้ ไม่ได้ผูกดิดกับ datasource ที่เป็นอันเดียวกันกับใน jasper
    // ในการเทสโดย jasper studio เอง เราก็สามารถ mock เป็น db มาเทสได้เหมือนกัน ขอแค่มี field เหมือนกัน
    // font TH Sarabun New
    // ใช้ไฟล์ .jasper จากใน root เป็น String
    // ต้อง compile จาก .jrxml เป็น jasper เอง ผ่าน JasperStudio
    // ไม่ต้อง restart project
    // เพื่อแก้ปัญหาคือไม้เอก และสระอื่น ๆ ลอย สูงกว่าที่ควรเป็น จะใช้ ThaiExporterManager
    @GetMapping(path = "/11")
    public void hello11(HttpServletResponse response, @RequestParam String headerName) throws JRException, IOException, SQLException {
        reportService.hello11(response, headerName);
    }

}
